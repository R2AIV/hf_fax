#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <vector>

#define PI 3.141592

FILE *WaveFile;

typedef struct
{
	uint32_t chunkId;
	uint32_t chunkSize;
	uint32_t format;
	uint32_t subchunk1Id;
	uint32_t subchunk1Size;
	uint16_t audioFormat;
	uint16_t numChannels;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint16_t blockAlign;
	uint16_t bitsPerSample;
	uint32_t subchunk2Id;
	uint32_t subchunk2Size;
} WavHeader;

uint8_t WaveDataBuffer[13230000];

/*
 * Writing frequency to waveform buffer
 * Freq - frequency (Hz)
 * Msec - time (ms)
 * WaveFilePos - current position in buffer
 * OutBuffer - output buffer pointer
 *
 * Returns current position in waveform buffer
 */
uint32_t GenFreq(uint16_t Freq, uint16_t Msec, uint32_t WaveFilePos, uint8_t *OutBuffer)
{
	uint32_t TmpPos;
	uint32_t Pos;
	uint8_t TmpBuf[Msec * 45]; // 1 sec/44100
	float Angle = 0.0;

	for(TmpPos = 0; TmpPos < Msec * 44100 / 1000; TmpPos++)
	{
		Angle = Freq / 44100.0 * TmpPos * PI*2;
		TmpBuf[TmpPos] = sin(Angle) * 128.0 + 128.0;
	};

	for(Pos = 0;Pos<TmpPos;Pos++)
	{
		OutBuffer[WaveFilePos + Pos] = TmpBuf[Pos];		
	}

	return WaveFilePos + Pos;
};

int main(void)
{
	WavHeader hdr;

	uint32_t WaveFilePos = 0;

	memcpy((void *)&hdr.chunkId,"RIFF",4);
	memcpy((void *)&hdr.format,"WAVE",4);
	memcpy((void *)&hdr.subchunk1Id, "fmt ",4);
	memcpy((void *)&hdr.subchunk2Id, "data",4);

	hdr.subchunk1Size = 16;
	hdr.numChannels = 1;
	hdr.sampleRate = 44100;
	hdr.byteRate = 44100;
	hdr.blockAlign = 1;
	hdr.bitsPerSample = 8;
	hdr.audioFormat = 1;

	memset((void *)WaveDataBuffer,0,sizeof(WaveDataBuffer));

	WaveFilePos = 0;
	for(int u=0;u<1000;u++)
	{	
		WaveFilePos = GenFreq(1500,50,WaveFilePos,WaveDataBuffer);
		WaveFilePos = GenFreq(2300,50,WaveFilePos,WaveDataBuffer);
	};

	printf("%d\r\n", WaveFilePos);

	hdr.subchunk2Size = WaveFilePos;

	WaveFile = fopen("test.wav","wb");

	fwrite((void *)&hdr, sizeof(hdr),1,WaveFile);
	fwrite((void *)WaveDataBuffer, WaveFilePos,1,WaveFile);
	fclose(WaveFile);

	return 0;
};

